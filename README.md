## To compile and run the web API:

1. Install Visual Studio 2022: https://visualstudio.microsoft.com/downloads/.
2. Install and enable the Visual Studio extension "Conveyor by Keyoti" which opens a tunnel to the web API. 
3. Clone the repository.
4. Navigate to the `api` directory inside the project: `cd api`.
5. Inside `appsettings.json`, add values for:
   * `JWT.Secret` (a HEX string).
   * `ChatGptApiKey` (the API key for the ChatGPT API).
   * `Admins.UserName` and `Admins.Password` (username and password of the administrator user).
6. Debug and run the solution.
7. Navigate to the `nlp` directory: `cd ../nlp`.
8. Start the FastAPI web API using an ASGI web server like Uvicorn: `uvicorn main:app`.



## To compile and run the Android application:

1. Install Android Studio: https://developer.android.com/studio.
2. Clone the repository.
3. Open the `android` directory in Android Studio.
4. Inside `server.properties`, add values for:
   * `API_URI_DEBUG` (the URL of the locally running web API).
5. Inside `app/src/res/xml/network_security_config.xml`, add values for:
   * `network-security-config/debug-overrides/trust-anchors/domain` (the IP address of the locally running web API).
6. Add a `.cer` certificate (exported using a browser) of the locally running web API called `my_ca.cer` inside `app/src/res/raw`.
7. Debug and run the project.

<br/><br/>

To actually work, the projects require various resource files (consisting of binary files like extracted landmarks, ML model files, 3D model files, etc.) which were not uploaded to the repository.